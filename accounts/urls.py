from django.urls import path
from .views import detail, log_out, signup


urlpatterns = [
    path("login/", detail, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", signup, name="signup"),
]
