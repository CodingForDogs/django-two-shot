from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import NewReceiptForm, CreateCategoryForm, CreateAccountForm

# Create your views here.
from .models import Receipt, Account, ExpenseCategory


@login_required
def detail(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipt_table.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = NewReceiptForm(request.POST)
        if form.is_valid():
            vendor = form.cleaned_data["vendor"]
            total = form.cleaned_data["total"]
            tax = form.cleaned_data["tax"]
            date = form.cleaned_data["date"]
            category = form.cleaned_data["category"]
            account = form.cleaned_data["account"]
            newReceipt = Receipt.objects.create(
                vendor=vendor,
                total=total,
                tax=tax,
                date=date,
                category=category,
                account=account,
            )
            newReceipt.purchaser = request.user
            newReceipt.save()
            return redirect("/receipts/", name="home")
    form = NewReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "new_receipt.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("/receipts/accounts/")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }
    return render(request, "create_account.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "expense_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "account_list.html", context)
