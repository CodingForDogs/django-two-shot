from django import forms
from .models import Receipt, ExpenseCategory, Account
from django.db import models


class NewReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ("vendor", "total", "tax", "date", "category", "account")


class CreateCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ("name",)
