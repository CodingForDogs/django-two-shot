from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LogInForm
from django.contrib.auth.models import User
from accounts.forms import SignUpForm

# Create your views here.


def detail(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("receipts/", name="home")
    form = LogInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def log_out(request):
    logout(request)
    return redirect("/accounts/login/", name="login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(username, password)
                login(request, user)
                return redirect("/receipts/")
    form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
