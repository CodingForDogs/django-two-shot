from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.
from .models import ExpenseCategory, Account, Receipt

# admin.site.register(ExpenseCategory)
# admin.site.register(Account)
# admin.site.register(Receipt)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    pass
